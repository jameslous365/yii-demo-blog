<div class="post">
    <div class="title">
        <?php echo CHtml::link(CHtml::encode($data->title), $data->url); ?>
    </div>
    <div class="author">
        posted by <?php echo $data->author->username . ' on ' . date('F j, Y', $data->create_time); ?>
    </div>
    <div class="content">
        <div>
            <?php
            if ($data->coverBehavior->hasImage())
                echo CHtml::image($data->coverBehavior->getUrl('small'), '', array('style' => 'float:left; margin-right: 10px;')); ?>
            <?php
            echo $data->content;
            ?>

            <br class="clear:both;">
        </div>
        <?php $photos = $data->galleryBehavior->getGalleryPhotos(); ?>
        <?php if (count($photos)): ?>
            <div>
                <?php foreach ($photos as $photo): ?>
                    <?php echo CHtml::link(CHtml::image($photo->getUrl('small')), $photo->getUrl('medium'), array('target' => '_blank')); ?>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>

    <div class="nav">
        <b>Tags:</b>
        <?php echo implode(', ', $data->tagLinks); ?>
        <br/>
        <?php echo CHtml::link('Permalink', $data->url); ?> |
        <?php echo CHtml::link("Comments ({$data->commentCount})", $data->url . '#comments'); ?> |
        Last updated on <?php echo date('F j, Y', $data->update_time); ?>
    </div>
</div>
