<?php

class m131116_151509_add_gallery_tables extends CDbMigration
{

    public function up() {
        $this->createTable( '{{gallery}}', array(
            'id' => 'pk',
            'versions_data' => 'text NOT NULL',
            'name' => 'boolean NOT NULL DEFAULT 1',
            'description' => 'boolean NOT NULL DEFAULT 1'
        ) );

        $this->createTable( '{{gallery_photo}}', array(
            'id' => 'pk',
            'gallery_id' => 'integer NOT NULL REFERENCES gallery(id)',
            'rank' => 'integer NOT NULL DEFAULT 0',
            'name' => 'string NOT NULL DEFAULT \'\'',
            'description' => 'text',
            'file_name' => 'string NOT NULL'
        ) );

    }

    public function down() {
        $this->dropTable( 'gallery_photo' );
        $this->dropTable( 'gallery' );
    }
}