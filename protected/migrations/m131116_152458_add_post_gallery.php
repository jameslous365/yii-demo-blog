<?php

class m131116_152458_add_post_gallery extends CDbMigration
{
    public function up()
    {
        $this->addColumn('{{post}}', 'gallery_id', 'integer NULL');
    }

    public function down()
    {
        $this->dropColumn('{{post}}', 'gallery_id');
    }

}